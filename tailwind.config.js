/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{html,js,ts,jsx,tsx,mdx}",
    "./components/**/*.{html,js,ts,jsx,tsx,mdx}",
    "./app/**/*.{html,js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        Space_Grotesk: ["Space_Grotesk", "sans-serif"],
        Poppins: ["Poppins", "sans-serif"],
        Oswald: ["Oswald", "sans-serif"],
      },
      colors: {
        "primary-creme": "#F9F7EE",
        "primary-violet": "#1A1B24",
        "modal-bg": "#21222F",
        "red-one": "#E30052",
        "red-two": "#ED3237",
        "purple-one": "#AA00FF",
        "purple-two": "#7454A9",
        "purple-three": "#A980EC",
        "purple-four": "#8A66C5",
        "purple-five": "#B186FE",
        "light-text": "#C7C7D7",
        "light-creame-pann": "#E3CFBF",
        "light-purple-pann": "#261937",
        "light-gray-pann": "#7D8188",
        "light-gray-pann-two": "#686565",
        "light-blue-hover-pann": "#F0F0F0",
        "lime-pann": "#C8EE44",
      },
    },
  },
  plugins: [],
};

"use client";
import React, { useRef, useState } from "react";
import careerBanner from "../../public/assets/images/ServicesPage/PerfectServiceBanner.webp";
import { careerData } from "@utils/ConstantPageData/Career";
import TaglineHeaderComponent from "@components/HomePage/TaglineHeaderComponent";
import CareersBenefits from "@components/CareersPage/CareersBenefits";
import CareersJobOpening from "@components/CareersPage/CareersJobOpening";
import JobOpeningModal from "@components/CareersPage/JobOpeningModal";
import BottomToTopComponent from "@components/ButtonComponent/BottomToTopComponent";

const Careers = () => {
  const ref = useRef(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleClick = () => {
    ref.current?.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <React.Fragment>
      <BottomToTopComponent />
      <TaglineHeaderComponent
        title={careerData.SectionOneHead}
        desc={careerData.SectionOneDesc}
        onClickAction={handleClick}
        imgSrc={careerBanner}
      />
      <CareersBenefits />
      <CareersJobOpening onClickButtonModal={showModal} refId={ref} />
      {isModalOpen && (
        <JobOpeningModal openModal={isModalOpen} handleCancel={handleCancel} />
      )}
    </React.Fragment>
  );
};

export default Careers;

import "@styles/globals.css";
import {
  spaceGrotesk,
  poppins,
  oswald,
  sportingGrotesque,
} from "../utils/fonts";
import Footer from "@components/Navigations/Footer";
import "@fortawesome/fontawesome-svg-core/styles.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { config } from "@fortawesome/fontawesome-svg-core";
import NavBar from "@components/Navigations/NavBar";
config.autoAddCss = false;

export const metadata = {
  title: "Penn Solutions",
  description:
    "Develop professional web and mobile application development services.",
};

const RootLayout = ({ children }) => {
  return (
    <html lang="en">
      <head>
        <link rel="shortcut icon" href="./favicon.png" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="./apple-touch-icon.png"
        />
      </head>
      <body
        className={`${spaceGrotesk.variable} ${poppins.variable} ${oswald.variable} ${sportingGrotesque.variable}`}
      >
        <NavBar />
        <div className="main">{/* <div className='gradient'/> */}</div>
        <div className="app">{children}</div>
        <Footer />
      </body>
    </html>
  );
};

export default RootLayout;

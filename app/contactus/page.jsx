"use client";
import React from "react";
import contactBanner from "../../public/assets/images/ContactUs/contactBanner.webp";
import { contactData } from "@utils/ConstantPageData/Contact";
import TaglineHeaderComponent from "@components/HomePage/TaglineHeaderComponent";
import GetInTouch from "@components/ContactPage/GetInTouch";
import GetInTouchMap from "@components/ContactPage/GetInTouchMap";
import BottomToTopComponent from "@components/ButtonComponent/BottomToTopComponent";

const ContactUs = () => {
  return (
    <React.Fragment>
      <BottomToTopComponent />
      <TaglineHeaderComponent
        title={contactData.SectionOneHead}
        desc={contactData.SectionOneDesc}
        onClickAction={() => console.log("Contact Page")}
        imgSrc={contactBanner}
      />
      <GetInTouch />
      <GetInTouchMap />
    </React.Fragment>
  );
};

export default ContactUs;

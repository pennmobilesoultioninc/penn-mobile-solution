import React, { useRef, useState } from "react";
import PropTypes from "prop-types";
import Slider from "@ant-design/react-slick";
import { homeSliderContentArr } from "@utils/ConstantPageData/Home";
import { Col, Row } from "antd";

const TeamCanTrustSlider = () => {
  const [oldSlide, setOldSlide] = useState(0);
  let sliderRef = useRef(null);
  const next = () => {
    sliderRef.slickNext();
  };
  const previous = () => {
    sliderRef.slickPrev();
  };

  const settings = {
    dots: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 500,
    arrows: false,
    centerMode: true,
    // centerPadding: "60px",
    slidesToShow: 2,
    slidesToScroll: 1,
    beforeChange: (current, next) => {
      setOldSlide(current);
    },
    afterChange: (current, prev) => {
      setOldSlide(current);
    },
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: "0px",
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <Row justify={"end"} className="relative" style={{ top: "-70%", right: 0 }}>
      <Col xs={24} md={14} lg={14} xl={14} className="md:pl-3">
        <div className="main-team-can-trust">
          <Slider
            ref={(slider) => {
              sliderRef = slider;
            }}
            {...settings}
          >
            {homeSliderContentArr.map((data) => {
              return (
                <React.Fragment key={data.id}>
                  <ReuseCardComponent title={data.title} desc={data.desc} />
                </React.Fragment>
              );
            })}
          </Slider>
        </div>
      </Col>
    </Row>
  );
};

const ReuseCardComponent = ({ title, desc }) => {
  return (
    <div className={"home-slider-box"}>
      <span className="typoStyles700_26_16">
        {title}{" "}
        <span className="typoStyles700_26_16" style={{ color: "#565768" }}>
          {desc}
        </span>
      </span>
    </div>
  );
};

ReuseCardComponent.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
};

export default TeamCanTrustSlider;

import React from "react";
import { Col, Row } from "antd";
import Image from "next/image";
import semiSphere from "../../public/assets/icons/Semisphere.svg";
import teamTrust from "../../public/assets/images/HomePage/TeamTrust.webp";
import { homeData } from "@utils/ConstantPageData/Home";
import TeamCanTrustSlider from "./TeamCanTrustSlider";

const TeamCanTrust = () => {
  return (
    <React.Fragment>
      <Row className="px-4 md:px-10 lg:px-32 pt-14 pb-40 md:py-32 relative bg-primary-creme">
        <Image
          src={semiSphere}
          width={110}
          height={110}
          className="absolute right-0 top-0 hidden md:block"
          alt={homeData.SectionFiveHead}
          title={homeData.SectionFiveHead}
        />
        <Col
          xs={24}
          md={10}
          lg={10}
          xl={10}
          className="flex justify-center md:justify-start px-9 md:px-0 md:pr-8 lg:pr-24 mb-8 md:mb-0"
        >
          <Image
            src={teamTrust}
            className="w-full h-50"
            alt={homeData.SectionFiveHead}
            title={homeData.SectionFiveHead}
            loading="eager"
            layout="responsive"
          />
        </Col>
        <Col xs={24} md={14} lg={14} xl={14} className="flex items-center">
          <div className="w-full">
            <h4 className="typoStyles700_42 mb-6 text-center md:text-left">
              {homeData.SectionFiveHead}
            </h4>
            <div className="w-full">
              <p className="typoStyles400_28_16 text-light-gray-pann-two  text-center md:text-left px-1 md:px-0">
                {homeData.SectionFiveDesc}
              </p>
            </div>
          </div>
        </Col>
        <div className="spreadBox-color-trust"></div>
      </Row>
      <div className="pb-12 home-team-can-trust-gradient h-64">
        <TeamCanTrustSlider />
      </div>
    </React.Fragment>
  );
};

export default TeamCanTrust;

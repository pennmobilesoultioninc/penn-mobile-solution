import React from "react";
import { CloseCircleFilled } from "@ant-design/icons";
import { Modal } from "antd";
import Image from "next/image";
import modalBanner from "../../public/assets/images/Career/modalBanner.webp";
import Link from "next/link";

const JobOpeningModal = (props) => {
  const { openModal, handleCancel } = props;
  return (
    <Modal
      title="Basic Modal"
      open={openModal}
      onCancel={handleCancel}
      centered
      maskClosable={false}
      width={581}
      footer={null}
      title={""}
      className="modal-job"
      closeIcon={
        <CloseCircleFilled style={{ fontSize: 30, color: "#000000" }} />
      }
    >
      <div className="w-full">
        <div className="career-modal-banner">
          <Image
            src={modalBanner}
            className="w-full h-32"
            layout="responsive"
            loading="eager"
            style={{ objectFit: "contain" }}
            alt={"Pann Solution Dream Job"}
            title={"Pann Solution Dream Job"}
          />
        </div>
      </div>
      <div className="w-full px-5 py-10 md:px-8 md:py-8 bg-modal-bg">
        <h4 className="typoStylesPoppins700_24 text-white mb-4">
          Apply for Your Dream Job Today!
        </h4>
        <p className="typoStyles400_22_16 text-white mb-4">
          Embarking on a new career path is an exciting endeavor, and we're
          delighted that you're considering joining our team at Penn Mobile
          Soultions. To initiate the application process:
        </p>
        <p className="typoStyles400_22_16 text-white mb-4">
          Mail us your resume, portfolio, and mention the job title in the
          subject. We will get back to you.
        </p>
        <Link
          href={"mailto:hr@pennmobilesolutions.com"}
          className="typoStylesPoppins700_24 text-lime-pann"
        >
          <h5 className="typoStylesPoppins700_24 text-lime-pann">
            EMAIL: hr@pennmobilesolutions.com
          </h5>
        </Link>
      </div>
    </Modal>
  );
};

export default JobOpeningModal;

import React from "react";
import { Col, Row } from "antd";
import Image from "next/image";
import semiSphere from "../../public/assets/icons/Semisphere.svg";
import ProfessionalBenefits from "../../public/assets/images/Career/ProfessionalBenefits.webp";
import TrainingBenefits from "../../public/assets/images/Career/TrainingBenefits.webp";
import { careerData } from "@utils/ConstantPageData/Career";

const CareersBenefits = () => {
  return (
    <div className="pt-16 md:px-16 md:py-16 relative bg-primary-creme">
      <Image
        src={semiSphere}
        width={110}
        height={110}
        className="absolute right-0 top-0 hidden md:block"
        alt={careerData.SectionTwoHead}
        title={careerData.SectionTwoHead}
      />
      <div className="w-full">
        <h4 className="typoStyles500_60 md:mb-7 text-center">
          {careerData.SectionTwoHead}
        </h4>
      </div>
      <div className="mt-7 md:mt-14 justify-center relative">
        <div className="spreadBox-color-career"></div>
        <Row justify={"center"}>
          <Col xs={24} xl={12} className="flex md:p-2">
            <div className="w-full md:pb-16">
              <div className="w-full h-50">
                <Image
                  src={ProfessionalBenefits}
                  width={"100%"}
                  height={"100%"}
                  layout="responsive"
                  alt={careerData.SectionTwoSubHead}
                  title={careerData.SectionTwoSubHead}
                />
              </div>
              <div
                className="bg-purple-four py-8 md:py-14 px-5 md:pl-10 md:pr-8"
                style={{ height: "500px" }}
              >
                <h5 className="typoStyles700_28 mb-8 text-light-purple-pann">
                  {careerData.SectionTwoSubHead}
                </h5>
                <p className="typoStyles400_18 mb-8 text-white">
                  {careerData.SectionTwoSubDesc}
                </p>
                <ul>
                  {careerData.SectionTwoSubDescArr?.map((item, index) => {
                    return (
                      <React.Fragment key={index}>
                        <li className="typoStyles400_28_16 text-white">
                          &#8226;{"  "}
                          <span className="typoStyles400_28_16 text-white">
                            {item}
                          </span>
                        </li>
                      </React.Fragment>
                    );
                  })}
                </ul>
              </div>
            </div>
          </Col>
          <Col xs={24} xl={12} className="flex md:p-2">
            <div className="w-full md:pb-16">
              <div className="w-full h-50">
                <Image
                  src={TrainingBenefits}
                  width={"100%"}
                  height={"100%"}
                  layout="responsive"
                  alt={careerData.SectionTwoSubHeadTwo}
                  title={careerData.SectionTwoSubHeadTwo}
                />
              </div>
              <div
                className="bg-light-blue-hover-pann py-8 md:py-14 px-5 md:pl-10 md:pr-20"
                style={{ height: "500px" }}
              >
                <h5 className="typoStyles700_28 mb-8 text-light-purple-pann">
                  {careerData.SectionTwoSubHeadTwo}
                </h5>
                <p className="typoStyles400_18 mb-4 text-light-gray-pann-two">
                  {careerData.SectionTwoSubDescTwo}
                </p>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default CareersBenefits;

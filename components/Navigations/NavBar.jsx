"use client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import Logo from "../../public/assets/icons/logo.webp";
import openMenu from "../../public/assets/icons/openMenu.svg";
import { CloseCircleFilled } from "@ant-design/icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRightLong } from "@fortawesome/free-solid-svg-icons";
import { usePathname } from "next/navigation";

const NavBar = () => {
  const pathname = usePathname();

  const [navbar, setNavbar] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", function () {
      let header = document.querySelector("header");
      header.classList.toggle("sticky", window.scrollY > 0);
    });
  }, []);

  return (
    <header className="bg-primary-violet">
      <Link href="/" className={`${navbar ? "mobile-logo-section" : ""}`}>
        <Image
          src={Logo}
          loading="eager"
          className="logo-brand"
          alt={"Pann Solutions"}
          title={"Pann Solutions"}
        />
      </Link>
      <div className="md:hidden">
        <button
          className={`p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border ${
            navbar ? "mobile-open-closed-section" : ""
          }`}
          onClick={() => setNavbar(!navbar)}
        >
          {navbar ? (
            <CloseCircleFilled style={{ fontSize: 40, color: "#fff" }} />
          ) : (
            <Image
              src={openMenu}
              width={40}
              height={40}
              alt="Mobile Open Icon"
              title="Mobile Open Icon"
              className="focus:border-none active:border-none"
            />
          )}
        </button>
      </div>
      <div
        className={`w-full flex items-center justify-center md:justify-end md:block md:pb-0 md:mt-0 ${
          navbar ? "md:p-0 block" : "hidden"
        }`}
      >
        <ul
          className={`w-full h-screen md:h-auto items-center justify-center md:justify-end md:flex ${
            navbar ? "mobile-link-ul" : ""
          }`}
        >
          <li
            className={`typoStyles400_14 ${
              pathname.toString() === "/"
                ? "text-white border-b-2 border-white"
                : "text-white"
            } text-center z-10 hover:bg-red-two md:hover:text-red-two md:hover:bg-transparent pb-1 mx-4 ${
              navbar ? "mobile-link-li" : ""
            }`}
          >
            <Link href="/" className="m-0" onClick={() => setNavbar(!navbar)}>
              Home
            </Link>
          </li>
          <li
            className={`typoStyles400_14 ${
              pathname.toString() === "/services"
                ? "text-white border-b-2 border-white"
                : "text-white"
            } text-center z-10 hover:bg-red-two md:hover:text-red-two md:hover:bg-transparent pb-1 mx-4 ${
              navbar ? "mobile-link-li" : ""
            }`}
          >
            <Link
              href="/services"
              className="m-0"
              onClick={() => setNavbar(!navbar)}
            >
              Services
            </Link>
          </li>
          <li
            className={`typoStyles400_14 ${
              pathname.toString() === "/careers"
                ? "text-white border-b-2 border-white"
                : "text-white"
            } text-center z-10 hover:bg-red-two md:hover:text-red-two md:hover:bg-transparent pb-1 mx-4 ${
              navbar ? "mobile-link-li" : ""
            }`}
          >
            <Link
              href="/careers"
              className="m-0"
              onClick={() => setNavbar(!navbar)}
            >
              Careers
            </Link>
          </li>
          <li
            className={`py-2 rounded-md ${
              pathname.toString() === "/contactus" ? "bg-red-two" : "bg-white"
            } shadow typoStyles400_14 z-10 text-black text-center border-b-2 md:border-b-0 hover:bg-red-two md:hover:text-white md:hover:bg-red-two md:ml-4 ${
              navbar ? "mobile-link-li" : ""
            }`}
          >
            <Link
              href="/contactus"
              onClick={() => setNavbar(!navbar)}
              id={"button-style"}
              className={`${
                pathname.toString() === "/contactus"
                  ? "text-white"
                  : "text-black"
              } md:hover:text-white`}
            >
              Contact Us{" "}
              <FontAwesomeIcon
                icon={faArrowRightLong}
                className="fas faArrowRightLong hover:bg-red-two  border-red-two  md:hover:text-white md:hover:bg-red-two"
                style={{ fontSize: "1rem" }}
              />
            </Link>
          </li>
        </ul>
      </div>
    </header>
  );
};

export default NavBar;

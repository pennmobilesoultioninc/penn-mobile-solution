import React from "react";
import { footerConst } from "@utils/ConstantPageData/FooterConstantData";
import { Col, Row } from "antd";
import { PlusOutlined, MinusOutlined } from "@ant-design/icons";
import Marquee from "react-fast-marquee";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faTwitter,
  faInstagram,
  faLinkedin,
  faLinkedinIn,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faPhone, faHome } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
//

const Footer = () => {
  return (
    <React.Fragment>
      <Row
        justify={"space-around"}
        style={{ padding: "1rem 0", backgroundColor: "#251935" }}
      >
        <Marquee direction={"left"} pauseOnHover={true} speed={100}>
          <h4 className="typoStylesSportingGrotesque_700_24 text-white flex items-center">
            <MinusOutlined
              style={{
                fontSize: "40px",
                margin: "0 15px 8px 15px",
                color: "#A980EC",
              }}
            />{" "}
            THANK YOU FOR YOUR VISIT{" "}
            <PlusOutlined
              style={{
                fontSize: "36px",
                margin: "0 15px 15px 15px",
                color: "#A980EC",
              }}
            />
            THANK YOU FOR YOUR VISIT{" "}
            <MinusOutlined
              style={{
                fontSize: "40px",
                margin: "0 15px 8px 15px",
                color: "#A980EC",
              }}
            />{" "}
            THANK YOU FOR YOUR VISIT{" "}
          </h4>
        </Marquee>
      </Row>
      <Row
        justify={"center"}
        className="bg-primary-violet relative pt-8 pb-16 px-5 md:py-16 md:px-3"
        // style={{ padding: "3.75rem 0.938rem" }}
      >
        <Col xs={24} md={22} lg={20} xl={20}>
          <Row className="pb-4" style={{ borderBottom: "2px solid #C7C7D7" }}>
            <Col xs={24} md={12} lg={12} xl={12}>
              <p className="typoStylesSportingGrotesque_700_24 text-white text-center md:text-left">
                {footerConst.organizationName}
              </p>
            </Col>
            <Col
              xs={24}
              md={12}
              lg={12}
              xl={12}
              className="flex justify-center mt-6 md:mt-0 md:justify-end"
            >
              {/* <div className="f-icon-button mr-3">
                <FontAwesomeIcon
                  icon={faFacebookF}
                  className="fas faFacebookF"
                  style={{ color: "#ffffff", fontSize: "1.9rem" }}
                />
              </div>
              <div className="f-icon-button mr-3">
                <FontAwesomeIcon
                  icon={faTwitter}
                  className="fas faTwitter"
                  style={{ color: "#ffffff", fontSize: "1.9rem" }}
                />
              </div>
              <div className="f-icon-button">
                <FontAwesomeIcon
                  icon={faInstagram}
                  className="fas faInstagram"
                  style={{ color: "#ffffff", fontSize: "1.9rem" }}
                />
              </div> */}
              <Link
                href={footerConst.organizationLinkedIn}
                target="_blank"
                className="f-icon-button z-10"
              >
                <FontAwesomeIcon
                  icon={faLinkedin}
                  className="fas faLinkedin pt-1"
                  style={{ color: "#ffffff", fontSize: "1.5rem" }}
                />
              </Link>
            </Col>
          </Row>
          <Row className="mt-8">
            <Col xs={24} md={7} lg={7} xl={7}>
              <div className="flex items-center justify-center md:justify-start">
                <FontAwesomeIcon
                  icon={faHome}
                  className="fas faHome"
                  style={{ color: "#ffffff", fontSize: "1.125rem" }}
                />
                <p className="typoStyles700_18 text-white ml-3">
                  Philadelphia Office
                </p>
              </div>
              <p className="typoStyles400_28_16 mt-4 text-light-text px-12 md:p-0 text-center md:text-left">
                {footerConst.organizationAddress}
              </p>
            </Col>
            <Col
              xs={24}
              md={4}
              lg={4}
              xl={4}
              offset={3}
              className="hidden md:block"
            >
              <div className="flex items-center">
                <FontAwesomeIcon
                  icon={faPhone}
                  className="fas faPhone"
                  style={{ color: "#ffffff", fontSize: "1.125rem" }}
                />
                <p className="typoStyles700_18 text-white ml-3 hidden	md:block">
                  Phone
                </p>
              </div>
              <p className="typoStyles400_28_16 mt-4 text-light-text">
                {footerConst.organizationNumber}
              </p>
            </Col>
            <Col
              xs={24}
              md={4}
              lg={4}
              xl={4}
              offset={4}
              className="hidden md:block"
            >
              <div className="flex items-center">
                <FontAwesomeIcon
                  icon={faEnvelope}
                  className="fas faEnvelope"
                  style={{ color: "#ffffff", fontSize: "1.125rem" }}
                />
                <p className="typoStyles700_18 text-white ml-3">Email</p>
              </div>
              <p className="typoStyles400_28_16 mt-4 text-light-text break-normal md:break-words lg:break-normal">
                {footerConst.organizationEmail}
              </p>
            </Col>
            <Col xs={24} className="block md:hidden mt-6">
              <div className="flex items-center justify-center">
                <FontAwesomeIcon
                  icon={faPhone}
                  className="fas faPhone"
                  style={{ color: "#ffffff", fontSize: "1.125rem" }}
                />
                <p className="typoStyles400_28_16 text-light-text ml-3">
                  {footerConst.organizationNumber}
                </p>
              </div>
            </Col>
            <Col xs={24} className="block md:hidden mt-6">
              <div className="flex items-center justify-center">
                <FontAwesomeIcon
                  icon={faEnvelope}
                  className="fas faEnvelope"
                  style={{ color: "#ffffff", fontSize: "1.125rem" }}
                />
                <p className="typoStyles400_28_16 text-light-text ml-3">
                  {footerConst.organizationEmail}
                </p>
              </div>
            </Col>
          </Row>
        </Col>
        <div className="home-footer-spread-bottom-pink hidden md:block"></div>
        <div className="home-spread-bottom-pink hidden md:block"></div>
      </Row>
      <Row
        className="bg-primary-creme"
        style={{ padding: "1.625rem 0.938rem" }}
      >
        <Col span={24}>
          <p className="typoStyles500_16 text-center">
            {footerConst.copyRights}
          </p>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default Footer;

import React, { useRef, useState } from "react";
import PropTypes from "prop-types";
import Slider from "@ant-design/react-slick";
import { ServiceSectionFourHeadArr } from "@utils/ConstantPageData/Services";
import Image from "next/image";
import planningImg from "../../public/assets/icons/planning.svg";
import designImg from "../../public/assets/icons/design.svg";
import developmentImg from "../../public/assets/icons/development.svg";
import activeLeft from "../../public/assets/icons/Arrow-right-Disable.svg";
import activeRight from "../../public/assets/icons/Arrow-right-active.svg";

const ServicePageMobileCarousel = () => {
  const [oldSlide, setOldSlide] = useState(0);

  let sliderRef = useRef(null);
  const next = () => {
    sliderRef.slickNext();
  };
  const previous = () => {
    sliderRef.slickPrev();
  };
  const settings = {
    dots: false,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 500,
    infinite: true,
    arrows: false,
    centerMode: true,
    fade: true,
    cssEase: "linear",
    slidesToShow: 1,
    beforeChange: (current, next) => {
      setOldSlide(current);
    },
    afterChange: (current, prev) => {
      setOldSlide(current);
    },
    responsive: [
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: "0px",
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <div className="my-12 service-page-slider-mobile">
      <Slider
        ref={(slider) => {
          sliderRef = slider;
        }}
        {...settings}
      >
        {ServiceSectionFourHeadArr.map((data) => {
          return (
            <React.Fragment key={data.id}>
              <ReuseCardComponent
                dataInfo={data}
                currentSlide={oldSlide}
                imgSrc={
                  data.id === 0
                    ? planningImg
                    : data.id === 1
                    ? designImg
                    : developmentImg
                }
                title={data.title}
                desc={data.description}
              />
            </React.Fragment>
          );
        })}
      </Slider>
      <div className="text-center mt-9">
        <button className="button mr-9" onClick={previous}>
          <Image src={activeLeft} layout="responsive" alt="Left Arrow" />
        </button>
        <button className="button" onClick={next}>
          <Image src={activeRight} layout="responsive" alt="Right Arrow" />
        </button>
      </div>
    </div>
  );
};

const ReuseCardComponent = ({
  dataInfo,
  currentSlide,
  imgSrc,
  title,
  desc,
}) => {
  return (
    <div
      className={`card-box-style-mobile`}
      style={{
        backgroundColor: `${
          dataInfo.id === currentSlide ? "#8A66C5" : "#F0F0F0"
        }`,
        border: `1px solid ${
          dataInfo.id === currentSlide ? "#8A66C5" : "#F0F0F0"
        }`,
      }}
    >
      <div className="w-full flex justify-center">
        <Image
          src={imgSrc}
          className="w-20 h-20 text-center"
          loading="eager"
          // layout="responsive"
          alt={title}
          title={title}
        />
      </div>
      <h4 className="typoStyles700_42 text-black my-3">{title}</h4>
      <h4
        className={`typoStyles400_14 ${
          dataInfo.id === currentSlide
            ? "text-white"
            : "text-light-gray-pann-two"
        }`}
      >
        {desc}
      </h4>
    </div>
  );
};

ReuseCardComponent.propTypes = {
  dataInfo: PropTypes.array || PropTypes.object,
  currentSlide: PropTypes.number,
  imgSrc: PropTypes.string || PropTypes.object,
  title: PropTypes.string,
  desc: PropTypes.string,
};

export default ServicePageMobileCarousel;

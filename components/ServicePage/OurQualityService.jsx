import React from "react";
import PropTypes from "prop-types";
import {
  serviceData,
  ServiceSectionTwoHeadArr,
} from "@utils/ConstantPageData/Services";
import { Col, Row } from "antd";
import Image from "next/image";
import semiSphere from "../../public/assets/icons/Semisphere.svg";
//

const OurQualityService = () => {
  return (
    <div className="flex justify-center service-our-quality bg-primary-creme relative">
      <Image
        src={semiSphere}
        width={110}
        height={110}
        className="absolute right-0 top-0 hidden md:block"
        alt={serviceData.SectionTwoHead}
        title={serviceData.SectionTwoHead}
      />
      <div className="w-full md:w-5/6">
        <h4 className="typoStyles500_60 mb-7 md:mb-14 text-center">
          {serviceData.SectionTwoHead}
        </h4>
        <div className="w-full px-4 md:px-0 md:w-4/5  mx-auto">
          <p className="typoStyles400_20 text-center text-light-gray-pann-two">
            {serviceData.SectionTwoDesc}
          </p>
        </div>
        <div className="md:my-24 justify-center relative">
          <div className="spreadBox-color"></div>
          <Row>
            {ServiceSectionTwoHeadArr.map((data) => {
              return (
                <React.Fragment key={data.id}>
                  <ReuseCardComponent
                    sno={data.number}
                    title={data.title}
                    desc={data.description}
                    headText={data?.headObj?.headTitle}
                    arrData={data?.headObj?.pointsArr}
                  />
                </React.Fragment>
              );
            })}
          </Row>
        </div>
      </div>
    </div>
  );
};

const ReuseCardComponent = ({ sno, title, desc, headText, arrData }) => {
  return (
    <Col
      xs={24}
      xl={8}
      className={`py-8 md:py-14 ${
        sno.toString() === "01" || sno.toString() === "02"
          ? "px-8"
          : "px-8 md:pl-8 md:pr-12"
      } hover:bg-light-blue-hover-pann`}
    >
      <h5 className="typoStyles_poppins_500_60 text-red-two">{sno}</h5>
      <h4 className="typoStyles700_24 mt-1 mb-4">{title}</h4>
      <p
        className="typoStyles400_16 text-light-gray-pann-two mb-6"
        style={{ width: `${sno.toString() === "03" ? "100%" : "85%"}` }}
      >
        {desc}
      </p>
      <h6 className="typoStyles700_26_16 text-light-gray-pann-two mb-6">
        {headText}
      </h6>
      <ul>
        {arrData?.map((item, index) => {
          return (
            <React.Fragment key={index}>
              <li className="typoStyles400_28_16">
                &#8226;{" "}
                <span className="typoStyles400_28_16 text-light-gray-pann-two">
                  {item}
                </span>
              </li>
            </React.Fragment>
          );
        })}
      </ul>
    </Col>
  );
};

ReuseCardComponent.propTypes = {
  sno: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  headText: PropTypes.string,
  arrData: PropTypes.array,
};

export default OurQualityService;

import React from "react";
import Image from "next/image";
import { Col, Row } from "antd";
import PropTypes from "prop-types";
import {
  serviceData,
  ServiceSectionThreeHeadArr,
} from "@utils/ConstantPageData/Services";
import iconArrow from "../../public/assets/icons/Arrow-Down-Disable.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinus } from "@fortawesome/free-solid-svg-icons";

const OurProcess = (props) => {
  const { activeCount, onClickAction } = props;
  return (
    <Row className="bg-primary-violet py-16 md:py-24 md:px-12 lg:px-36">
      {/* //  mainPaddingLeftRight148 mainPaddingTopBottom100 */}
      <Col span={24} className="px-5 md:px-0">
        <span className="typoStyles500_16 text-white flex items-center align-middle">
          <FontAwesomeIcon
            icon={faMinus}
            className="fas faMinus mr-4"
            style={{ color: "#ffffff", fontSize: "1.525rem" }}
          />
          {serviceData.SectionThreeDesc}
        </span>
        <h4 className="typoStyles700_42 text-white pt-5">
          {serviceData.SectionThreeHead}
        </h4>
      </Col>
      <Col span={24} className="mt-8 md:mt-32">
        {ServiceSectionThreeHeadArr.map((data, index) => {
          return (
            <React.Fragment key={index}>
              <CommonCardComponent
                activeCount={activeCount}
                id={data.id}
                sno={data.number + "."}
                title={data.title}
                desc={data.description}
                clickableBtn={onClickAction(data)}
              />
            </React.Fragment>
          );
        })}
        <div className="mt-16 bg-transparent flex items-center justify-end border-none">
          <button
            className="flex items-center typoStyles700_16 text-white  bg-transparent border-none"
            onClick={() => console.log("View more Process")}
          >
            View more
            <Image
              src={iconArrow}
              className="w-11 h-11 md:w-24 md:h-24"
              alt="View More"
              title="View More"
            />
          </button>
        </div>
      </Col>
    </Row>
  );
};

const CommonCardComponent = ({
  activeCount,
  id,
  clickableBtn,
  sno,
  title,
  desc,
}) => {
  return (
    <Row
      onClick={clickableBtn}
      className={`service-Our-processBox cursor-pointer px-10 md:px-5 py-10 md:py-10 lg:py-0 ${
        +activeCount === +id ? "bg-purple-four" : ""
      }`}
    >
      <Col
        xs={24}
        md={4}
        lg={4}
        xl={4}
        className="flex items-center justify-start"
      >
        <h5
          className={`typoStyles700_72 mb-6 md:mb-0 ${
            +activeCount === +id ? "text-light-purple-pann" : "text-red-two"
          }`}
        >
          {sno}
        </h5>
      </Col>
      <Col
        xs={24}
        md={8}
        lg={8}
        xl={8}
        className="flex items-center justify-start"
      >
        <h4 className={`typoStyles700_22 mb-6 md:mb-0 text-white`}>{title}</h4>
      </Col>
      <Col
        xs={24}
        md={12}
        lg={12}
        xl={12}
        className="flex items-center justify-start"
      >
        <p
          className={`typoStyles500_18 mb-6 md:mb-0 ${
            +activeCount === +id ? "text-white" : "text-light-gray-pann"
          }`}
        >
          {desc}
        </p>
      </Col>
    </Row>
  );
};

OurProcess.propTypes = {
  activeCount: PropTypes.string || PropTypes.number,
  onClickAction: PropTypes.func.isRequired,
};

CommonCardComponent.propTypes = {
  activeCount: PropTypes.number,
  id: PropTypes.number,
  clickableBtn: PropTypes.func.isRequired,
  sno: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
};

export default OurProcess;
